from django.contrib import admin
from. models import carreraDJFS, cursoDJFS, AsignaturaDJFS

@admin.register(carreraDJFS)
class carreraDJFSAdmin(admin.ModelAdmin):

    list_display = ('idCarreraDJFS', 'nombreCarreraDJFS', 'directorCarreraDJFS', 'duracionCarreraDJFS', 'modalidadCarreraDJFS', 'logoCarreraDJFS')
    ordering = ['nombreCarreraDJFS']
    search_fields = ['nombreCarreraDJFS', 'directorCarreraDJFS']
    list_editable = ['directorCarreraDJFS', 'duracionCarreraDJFS', 'modalidadCarreraDJFS']
    list_display_links = ('nombreCarreraDJFS',)
    list_per_page = 1
    exclude = ('logoCarreraDJFS',)

@admin.register(cursoDJFS)
class CursoDJFSAdmin(admin.ModelAdmin):
    list_display = ('idCursoDJFS', 'nivelCursoDJFS', 'descripcionCursoDJFS', 'aulaCursoDJFS', 'cupoCursoDJFS', 'fechaInicioDJFS')
    ordering = ['nivelCursoDJFS']
    search_fields = ['nivelCursoDJFS', 'descripcionCursoDJFS', 'aulaCursoDJFS']
    list_editable = ['descripcionCursoDJFS', 'aulaCursoDJFS', 'cupoCursoDJFS', 'fechaInicioDJFS']
    list_display_links = ('nivelCursoDJFS',)
    list_per_page = 1
    exclude = ('cupoCursoDJFS',)

@admin.register(AsignaturaDJFS)
class AsignaturaDJFSAdmin(admin.ModelAdmin):
    list_display = ('idAsignaturaDJFS', 'nombreAsignaturaDJFS', 'creditosAsignaturaDJFS', 'fechaInicioAsignaturaDJFS', 'fechaFinalizacionAsignatura', 'profesorAsignaturaDJFS', 'silaboAsignatuaDJFS', 'codigoAsignaturaDJFS', 'semestreAsignaturaDJFS')
    ordering = ['nombreAsignaturaDJFS']
    search_fields = ['nombreAsignaturaDJFS', 'profesorAsignaturaDJFS']
    list_editable = ['creditosAsignaturaDJFS', 'fechaInicioAsignaturaDJFS', 'fechaFinalizacionAsignatura', 'profesorAsignaturaDJFS', 'silaboAsignatuaDJFS', 'codigoAsignaturaDJFS', 'semestreAsignaturaDJFS']
    list_display_links = ('nombreAsignaturaDJFS',)
    list_per_page = 1
    exclude = ('nombreAsignaturaDJFS',)

