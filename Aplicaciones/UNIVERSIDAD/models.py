from django.db import models

# Create your models here.
class carreraDJFS(models.Model):
    idCarreraDJFS=models.AutoField(primary_key=True)
    nombreCarreraDJFS=models.CharField(max_length=150)
    directorCarreraDJFS=models.CharField(max_length=150)
    duracionCarreraDJFS=models.CharField(max_length=150)
    modalidadCarreraDJFS=models.CharField(max_length=150)
    logoCarreraDJFS=models.FileField(upload_to='carreras', null=True,blank=True)
    #def __str__(self):
        #text="{0} ({1})"
        #return  texto.format(self.nombreAsignaturaDJFS,self.directorCarreraDJFS,self.duracionCarreraDJFS,self.modalidadDJFS), 

class cursoDJFS(models.Model):
    idCursoDJFS=models. AutoField(primary_key=True)
    nivelCursoDJFS=models.CharField(max_length=150)
    descripcionCursoDJFS=models.TextField()
    aulaCursoDJFS=models.CharField(max_length=150)
    cupoCursoDJFS=models.CharField(max_length=150)
    fechaInicioDJFS=models.DateField()
    carreraDJFS=models.ForeignKey(carreraDJFS, null=True,blank=True,on_delete=models.CASCADE)


class AsignaturaDJFS(models.Model):
    idAsignaturaDJFS=models.AutoField(primary_key=True)
    nombreAsignaturaDJFS=models.CharField(max_length=150)
    creditosAsignaturaDJFS=models.TextField()
    fechaInicioAsignaturaDJFS=models.DateField()
    fechaFinalizacionAsignatura=models.DateField()
    profesorAsignaturaDJFS=models.CharField(max_length=150)
    silaboAsignatuaDJFS=models.CharField(max_length=150)
    codigoAsignaturaDJFS=models.CharField(max_length=150)
    semestreAsignaturaDJFS=models.CharField(max_length=150)
    cursoDJFS=models.ForeignKey(cursoDJFS, null=True,blank=True,on_delete=models.CASCADE)
