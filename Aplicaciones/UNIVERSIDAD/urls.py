from django.urls import path
from . import views

urlpatterns=[
    path('', views.index, name='index'),
    path('carreras/',views.listadoCarreras, name='carreras'),
    path('guardarCarrera/',views.guardarCarrera),
    path('eliminarCarrera/<idCarreraDJFS>',views.eliminarCarrera),
    path('editarCarrera/<idCarreraDJFS>',views.editarCarrera),
    path('procesarActualizacionCarrera/',views.procesarActualizacionCarrera),

    path('curso/',views.listaCurso, name='curso'),
    path('guardarCurso/',views.guardarCurso),
    path('eliminarCurso/<idCursoDJFS>',views.eliminarCurso),
    path('editarCurso/<idCursoDJFS>',views.editarCurso),
    path('procesarActualizacionCurso/',views.procesarActualizacionCurso),

    path('asignaturas/',views.listaAsignatura, name='asignaturas'),
    path('guardarAsignatura/',views.guardarAsignatura),
    path('eliminarAsignatura/<idAsignaturaDJFS>',views.eliminarAsignatura),
    path('editarAsignatura/<idAsignaturaDJFS>',views.editarAsignatura),
    path('procesarActualizacionAsignatura/',views.procesarActualizacionAsignatura),

    path('listadoCorreo/', views.listadoCorreo, name='listadoCorreo'),


    path('vista1/', views.vista1, name='vista1'),

    path('enviar-correo', views.enviar_correo, name='enviar_correo'),

]
