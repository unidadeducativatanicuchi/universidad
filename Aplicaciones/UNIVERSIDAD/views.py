from django.shortcuts import render,redirect
from .models import carreraDJFS, cursoDJFS, AsignaturaDJFS
from django.contrib import messages

from django.shortcuts import HttpResponseRedirect
from django.core.mail import send_mail
from django.conf import settings


def carreras_index(request):
    # Lógica de la vista aquí
    return render(request, 'carreras/index.html', {})  # Asegúrate de que el nombre de la plantilla coincida con el que estás intentando renderizar

def listadoCorreo(request):
    if request.method == 'POST':
        # Aquí manejas las solicitudes POST
        return render(request, 'correo.html', {'mensaje': 'Datos recibidos correctamente'})
    else:
        # Aquí manejas las solicitudes GET
        return render(request, 'correo.html')


def vista1(request):
        return render(request, 'enviar_correo.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)

        messages.success(request, 'Se ha enviado tu correo')
        return HttpResponseRedirect('/vista1')

    return render(request, 'enviar_correo.html')

# Create your views here.
def index(request):
    return render(request,'index.html')

def listadoCarreras(request):
    carrerasBdd=carreraDJFS.objects.all()
    return render(request,'carrera.html',{'carreras':carrerasBdd})


def guardarCarrera(request):
    nombreCarreraDJFS=request.POST["nombreCarreraDJFS"]
    directorCarreraDJFS=request.POST["directorCarreraDJFS"]
    duracionCarreraDJFS=request.POST["duracionCarreraDJFS"]
    modalidadCarreraDJFS=request.POST["modalidadCarreraDJFS"]
    logoCarreraDJFS=request.FILES.get("logoCarreraDJFS")
    #insertando datos mediante el ORM de Django
    nuevaCarrera=carreraDJFS.objects.create(nombreCarreraDJFS=nombreCarreraDJFS,directorCarreraDJFS=directorCarreraDJFS,duracionCarreraDJFS=duracionCarreraDJFS,modalidadCarreraDJFS=modalidadCarreraDJFS,
    logoCarreraDJFS=logoCarreraDJFS)
    messages.success(request,'Carrera Guardada Exitosamente');
    return redirect('/carreras')

def eliminarCarrera(request,idCarreraDJFS):
    carreraEliminar=carreraDJFS.objects.get(idCarreraDJFS=idCarreraDJFS)
    carreraEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/carreras')

def editarCarrera(request,idCarreraDJFS):
    carreraEditar=carreraDJFS.objects.get(idCarreraDJFS=idCarreraDJFS)
    #clienteEditar.fotografia = request.FILES.get('fotografia')
    return render(request,'editarCarrera.html' ,{'carrera': carreraEditar})


def procesarActualizacionCarrera(request):
    idCarreraDJFS=request.POST["idCarreraDJFS"]
    nombreCarreraDJFS=request.POST["nombreCarreraDJFS"]
    directorCarreraDJFS=request.POST["directorCarreraDJFS"]
    duracionCarreraDJFS=request.POST["duracionCarreraDJFS"]
    modalidadCarreraDJFS=request.POST["modalidadCarreraDJFS"]

    if 'logoCarreraDJFS' in request.FILES:
            logoCarreraDJFS = request.FILES.get("logoCarreraDJFS")
    else:
            # Si no se proporciona una nueva imagen, conserva la existente
            carrera_existente = carreraDJFS.objects.get(idCarreraDJFS=idCarreraDJFS)
            logoCarreraDJFS = carrera_existente.logoCarreraDJFS


    #Insertando datos mediante el ORM de DJANGO
    carreraEditar=carreraDJFS.objects.get(idCarreraDJFS=idCarreraDJFS)
    carreraEditar.nombreCarreraDJFS=nombreCarreraDJFS
    carreraEditar.directorCarreraDJFS=directorCarreraDJFS
    carreraEditar.duracionCarreraDJFS=duracionCarreraDJFS
    carreraEditar.modalidadCarreraDJFS=modalidadCarreraDJFS
    carreraEditar.logoCarreraDJFS = logoCarreraDJFS 
    carreraEditar.save()
    messages.success(request,
      'Carrera ACTUALIZADO Exitosamente')
    return redirect('/carreras')

#curso
def listaCurso(request):
    cursosBdd=cursoDJFS.objects.all()
    carrerasBdd=carreraDJFS.objects.all()
    return render(request,'curso.html',{'cursos':cursosBdd, 'carreras':carrerasBdd})


def guardarCurso(request):
    if request.method == 'POST':
        # Capturando los valores del formulario
        id_carrera = request.POST["id_carrera"]
        # Capturando el tipo seleccionado por el usuario
        carreraSeleccionada = carreraDJFS.objects.get(idCarreraDJFS=id_carrera)
        
        nivelCursoDJFS = request.POST["nivelCursoDJFS"]
        descripcionCursoDJFS = request.POST["descripcionCursoDJFS"]
        aulaCursoDJFS = request.POST["aulaCursoDJFS"]
        cupoCursoDJFS = request.POST["cupoCursoDJFS"]
        fechaInicioDJFS = request.POST["fechaInicioDJFS"]

        # Insertando datos mediante el ORM de Django
        nuevoCurso = cursoDJFS.objects.create(
            nivelCursoDJFS=nivelCursoDJFS,
            descripcionCursoDJFS=descripcionCursoDJFS,
            aulaCursoDJFS=aulaCursoDJFS,
            cupoCursoDJFS=cupoCursoDJFS,
            fechaInicioDJFS=fechaInicioDJFS,
            carreraDJFS=carreraSeleccionada  # Asignando la carrera seleccionada al nuevo curso
        )

        messages.success(request, 'Curso guardado exitosamente')
        return redirect('/curso/')  # Redirige a la página de listado de cursos después de guardar el curso


def eliminarCurso(request,idCursoDJFS):
    cursoEliminar=cursoDJFS.objects.get(idCursoDJFS=idCursoDJFS)
    cursoEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/curso/')

def editarCurso(request,idCursoDJFS):
    cursoEditar=cursoDJFS.objects.get(idCursoDJFS=idCursoDJFS)
    carrerasBdd=carreraDJFS.objects.all()
    return render(request,'editarCurso.html' ,{'curso': cursoEditar,'carreras':carrerasBdd})


def procesarActualizacionCurso(request):
    idCursoDJFS=request.POST["idCursoDJFS"]
    id_carrera=request.POST["id_carrera"]
    carreraSeleccionado=carreraDJFS.objects.get(idCarreraDJFS=id_carrera)
    nivelCursoDJFS=request.POST["nivelCursoDJFS"]
    descripcionCursoDJFS=request.POST["descripcionCursoDJFS"]
    aulaCursoDJFS=request.POST["aulaCursoDJFS"]
    cupoCursoDJFS=request.POST["cupoCursoDJFS"]
    fechaInicioDJFS=request.POST["fechaInicioDJFS"]
  

    

    #Insertando datos mediante el ORM de DJANGO
    cursoEditar=cursoDJFS.objects.get(idCursoDJFS=idCursoDJFS)
    cursoEditar.carrera=carreraSeleccionado
    cursoEditar.nivelCursoDJFS=nivelCursoDJFS
    cursoEditar.descripcionCursoDJFS=descripcionCursoDJFS
    cursoEditar.aulaCursoDJFS=aulaCursoDJFS
    cursoEditar.cupoCursoDJFS=cupoCursoDJFS
    cursoEditar.fechaInicioDJFS=fechaInicioDJFS

    cursoEditar.save()
    messages.success(request,
      'Curso ACTUALIZADO Exitosamente')
    return redirect('/curso/')

    #asignatura
def listaAsignatura(request):
    asignaturasBdd=AsignaturaDJFS.objects.all()
    cursosBdd=cursoDJFS.objects.all()
    return render(request,'asignatura.html',{'asignaturas':asignaturasBdd, 'cursos':cursosBdd})


def guardarAsignatura(request):
    #Capturando los valores del formulario
    id_curso=request.POST["id_curso"]
    #scapturando el tipo seleccionado por el usuario
    cursoSeleccionado=cursoDJFS.objects.get(idCursoDJFS=id_curso)
    nombreAsignaturaDJFS=request.POST["nombreAsignaturaDJFS"]
    creditosAsignaturaDJFS=request.POST["creditosAsignaturaDJFS"]
    fechaInicioAsignaturaDJFS=request.POST["fechaInicioAsignaturaDJFS"]
    fechaFinalizacionAsignatura=request.POST["fechaFinalizacionAsignatura"]
    profesorAsignaturaDJFS=request.POST["profesorAsignaturaDJFS"]
    silaboAsignatuaDJFS=request.FILES["silaboAsignatuaDJFS"]
    codigoAsignaturaDJFS=request.POST["codigoAsignaturaDJFS"]
    semestreAsignaturaDJFS=request.POST["semestreAsignaturaDJFS"]

    #insertando datos mediante el ORM de Django
    nuevaAsignatura=AsignaturaDJFS.objects.create(nombreAsignaturaDJFS=nombreAsignaturaDJFS,
    creditosAsignaturaDJFS=creditosAsignaturaDJFS,fechaInicioAsignaturaDJFS=fechaInicioAsignaturaDJFS,
    fechaFinalizacionAsignatura=fechaFinalizacionAsignatura,profesorAsignaturaDJFS=profesorAsignaturaDJFS,
    silaboAsignatuaDJFS=silaboAsignatuaDJFS,codigoAsignaturaDJFS=codigoAsignaturaDJFS,semestreAsignaturaDJFS=semestreAsignaturaDJFS,
    cursoDJFS=cursoSeleccionado)
    messages.success(request,'Asignatura guardado exitosamente');
    return redirect('/asignaturas/')

def eliminarAsignatura(request,idAsignaturaDJFS):
    asignaturaEliminar=AsignaturaDJFS.objects.get(idAsignaturaDJFS=idAsignaturaDJFS)
    asignaturaEliminar.delete()
    messages.success(request,'Eliminado Exitosamente');
    return redirect('/asignaturas/')

def editarAsignatura(request,idAsignaturaDJFS):
    asignaturaEditar=AsignaturaDJFS.objects.get(idAsignaturaDJFS=idAsignaturaDJFS)
    cursosBdd=cursoDJFS.objects.all()
    return render(request,'editarAsignatura.html' ,{'asignatura': asignaturaEditar,'cursos':cursosBdd})


def procesarActualizacionAsignatura(request):
    idAsignaturaDJFS = request.POST["idAsignaturaDJFS"]
    id_curso = request.POST["id_curso"]
    cursoSeleccionado = cursoDJFS.objects.get(idCursoDJFS=id_curso)
    nombreAsignaturaDJFS = request.POST["nombreAsignaturaDJFS"]
    creditosAsignaturaDJFS = request.POST["creditosAsignaturaDJFS"]
    fechaInicioAsignaturaDJFS = request.POST["fechaInicioAsignaturaDJFS"]
    fechaFinalizacionAsignatura = request.POST["fechaFinalizacionAsignatura"]
    profesorAsignaturaDJFS = request.POST["profesorAsignaturaDJFS"]
    codigoAsignaturaDJFS = request.POST["codigoAsignaturaDJFS"]
    semestreAsignaturaDJFS = request.POST["semestreAsignaturaDJFS"]

    # Verificar si el archivo está presente en la solicitud
    if 'silaboAsignatuaDJFS' in request.FILES:
        silaboAsignatuaDJFS = request.FILES.get("silaboAsignatuaDJFS")
    else:   
        # Si no se proporciona un nuevo archivo PDF, conserva el existente
        asignatura_existente = AsignaturaDJFS.objects.get(idAsignaturaDJFS=idAsignaturaDJFS)
        silaboAsignatuaDJFS = asignatura_existente.silaboAsignatuaDJFS

    # Insertando datos mediante el ORM de Django
    asignaturaEditar = AsignaturaDJFS.objects.get(idAsignaturaDJFS=idAsignaturaDJFS)
    asignaturaEditar.curso = cursoSeleccionado
    asignaturaEditar.nombreAsignaturaDJFS = nombreAsignaturaDJFS
    asignaturaEditar.creditosAsignaturaDJFS = creditosAsignaturaDJFS
    asignaturaEditar.fechaInicioAsignaturaDJFS = fechaInicioAsignaturaDJFS
    asignaturaEditar.fechaFinalizacionAsignatura = fechaFinalizacionAsignatura
    asignaturaEditar.profesorAsignaturaDJFS = profesorAsignaturaDJFS
    asignaturaEditar.silaboAsignatuaDJFS = silaboAsignatuaDJFS
    asignaturaEditar.codigoAsignaturaDJFS = codigoAsignaturaDJFS
    asignaturaEditar.semestreAsignaturaDJFS = semestreAsignaturaDJFS

    asignaturaEditar.save()
    messages.success(request, 'Asignatura ACTUALIZADO Exitosamente')
    return redirect('/asignaturas/')
